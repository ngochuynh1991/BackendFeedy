@extends('layouts.master')

@section('main_content')
<section class="content-header">
    <h1>{{ trans('menu.list_articles') }}</h1>
    <a href="{{ URL::to('media/article/create') }}" class="btn btn-success">Tạo bài giới thiệu nhà hàng</a>
    <a href="{{ URL::to('media/recipe/create') }}" class="btn btn-success">Tạo công thức nấu ăn </a>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ trans('menu.media_zone') }}</li>
        <li class="active">{{ trans('menu.list_articles') }}</li>
    </ol>
</section>
<section class="content">
    <div>
        <div style="padding-bottom: 5px; margin-bottom: 5px; border-bottom: 1px solid #ddd">
            <form method="get" action="/media/article" autocomplete="off" role="form" class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control search_top" name="key" id="key"
                           value="{{ old('key') }}"
                           autocomplete="off" placeholder="{{ trans('article.search_by_title') }}"
                           style="width: 150px;">
                </div>
                <div class="form-group">
                    <div id="reportrange" class="btn btn-default "
                         style="position: relative; display: inline-block">
                        <i class="glyphicon glyphicon-calendar"></i>
                        @if(old('start_date') != null && old('end_date') != null)
                        <span id="time_select">{{date(old('start_date'))}}
                            - {{date(old('end_date')) }}</span>
                        @else
                        <span id="time_select">{{date('Y-m-01 00:00:00',time())}}
                            - {{date('Y-m-t 23:59:59',time()) }}</span>
                        @endif
                        <b class="caret"></b>
                        <input type="hidden" name="start_date" id="start_date" value="{{ old('start_date') }}">
                        <input type="hidden" name="end_date" id="end_date" value="{{ old('end_date') }}">
                    </div>
                </div>
                <div class="form-group">
                    <select class="form-control"  name="category" data-live-search="true" data-width="120px">
                        <option value="" @if(!isset($request_all['category'])){{"selected"}}@endif >{{ trans('article.category') }}</option>
                        @foreach( $category as $item )
                        <option value="{{ $item->id }}">{{ $item->title }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="status" data-live-search="true" style="width:100px;">
                        <option @if(!isset($request_all['status'])){{"selected"}}@endif value="">{{ trans('article.status') }}</option>
                        <option @if(isset($request_all['status']) && $request_all['status'] == "draft"){{"selected"}}@endif value="draft">{{ trans('article.draft') }}</option>
                        <option @if(isset($request_all['status']) && $request_all['status'] == "publish"){{"selected"}}@endif value="publish">{{ trans('article.published') }}</option>
                    </select>
                </div>
                <input type="submit" class="btn btn-danger" name="search" value="{{ trans('home.search') }}">
                </div>
                <div class="post-container">
                    <div class="box box-solid">
                        <div class="box-body no-padding">
                            <div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('article.title') }}</th>
                                            <th>{{ trans('article.author') }}</th>
                                            <th>{{ trans('article.categories') }}</th>
                                            <th>{{ trans('article.type') }}</th>
                                            <th>{{ trans('article.tag') }}</th>
                                            <th>{{ trans('article.created_at') }}</th>
                                            <th>{{ trans('article.view') }}</th>
                                            <th>{{ trans('article.approved_by') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($articles as $article)
                                        <tr id="" class="post-item post-id-{{$article['id']}}" >
                                            <td>
                                                <p>{{ $article['title'] }}</p>
                                                <?php
                                                if ($article['type'] === 'Review') {
                                                    ?>
                                                    <p><a href="/media/article/edit/{{ $article['id'] }}">{{ trans('article.edit') }}</a> &nbsp;&nbsp;<span onclick="javacript:deleteArt('{{ $article['id'] }}', 1);" >{{ trans('article.delete') }}</span> &nbsp;&nbsp;<span onclick="javacript:publishArt('{{ $article['id'] }}', 1);" >{{ trans('article.publish') }}</span></p>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <p><a href="/media/recipe/edit/{{ $article['id'] }}">{{ trans('article.edit') }}</a> &nbsp;&nbsp;<span onclick="javacript:deleteArt('{{ $article['id'] }}', 2);" >{{ trans('article.delete') }}</span> &nbsp;&nbsp;<span onclick="javacript:publishArt('{{ $article['id'] }}', 2);" >{{ trans('article.publish') }}</span></p>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                            <td><p>{{ $article['creator'] }}</p></td>
                                            <td><p>
                                                    @foreach($article->articleCategory as $value)
                                                    {{ $value->title }}
                                                    <!--{{ @str_slug($value->title) }}-->
                                                    @endforeach
                                                </p></td>
                                            <td><p>{{ $article['type'] }}</p></td>
                                            <td><p>{{ $article['tag'] }}</p></td>
                                            <td><p>{{ $article['created_at'] }}</p></td>
                                            <td><p>{{ $article['view'] }}</p></td>
                                            <td><p>{{ $article['approve_by'] }}</p></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <style>
                        table tr td p span{
                            cursor: pointer;
                            color: #3c8dbc;
                        }
                        table tr td p span:hover{
                            color:#72afd2;
                        }
                    </style>
                    {!! $articles->render() !!}
            </form>
        </div>
    </div>
</section>

<div class="modal fade" id="reviewArticleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div style="width: 70%;" class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="col-md-9 modal-title" id="exampleModalLabel">{{ trans('article.review_article') }}</h2>
                <button id="btn-status" style="width:130px;margin-left: 60px;" status="off"  id="summitToPublish" class="btn btn-success" type="button">
                    {{ trans('article.publish') }}
                </button>
            </div>
            <div class="modal-body" id="reviewArticleModalBody">

            </div>
        </div>
    </div>
</div>
@stop

@section('custom_header')
<link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
<link href="{{ asset('plugins/iCheck/minimal/blue.css') }}" rel="stylesheet">
@stop

@section('custom_footer')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('dist/js/module/article.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/iCheck/icheck.js') }}"></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script>
            function deleteArt(id, check){
            if (check === 1){
            link = "{{URL::to('/media/article/delete/')}}";
            } else{
            link = "{{URL::to('/media/recipe/delete/')}}";
            }
            $.ajax({
            type: 'POST',
                    url: link,
                    data: 'id=' + id,
                    success: function (obj) {
                    if (obj !== null) {
                    obj = $.parseJSON(obj);
                            if (obj.status === 'success'){
                    location.reload();
                    }
                    }
                    },
                    error: function (a, b, c) {
                    }
            });
            }
            
            function publishArt(id, check){
            if (check === 1){
            link = "{{URL::to('/media/article/publish/')}}";
            } else{
            link = "{{URL::to('/media/recipe/publish/')}}";
            }
            $.ajax({
            type: 'POST',
                    url: link,
                    data: 'id=' + id,
                    success: function (obj) {
                    if (obj !== null) {
                    obj = $.parseJSON(obj);
                            if (obj.status === 'success'){
                    location.reload();
                    }
                    }
                    },
                    error: function (a, b, c) {
                    }
            });
            }
            
            $(document).ready(function () {
            $('#reportrange').daterangepicker(
            {
            ranges: {
            '{{ trans('article.today') }}': [moment(), moment()],
                    '{{ trans('article.yesterday') }}': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '{{ trans('article.last_7_day') }}': [moment().subtract(6, 'days'), moment()],
                    '{{ trans('article.last_30_day') }}': [moment().subtract(29, 'days'), moment()],
                    '{{ trans('article.this_month') }}': [moment().startOf('month'), moment().endOf('month')],
                    '{{ trans('article.last_month') }}': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
                    startDate: moment('{{ date('Y - m - 01 00:00:00', time()) }}'),
                    endDate: moment('{{ date('Y - m - t 23:59:59', time()) }}')
            },
                    function (start, end) {
                    $('#reportrange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
                            $('#start_date').val(start.format('YYYY-MM-DD HH:mm:ss'));
                            $('#end_date').val(end.format('YYYY-MM-DD HH:mm:ss'));
                    }
            );
                    var timeSelect = moment('{{date('Y - m - 01 00:00:00', time())}}').format('D MMMM, YYYY') + ' - ' + moment('{{date('Y - m - t 23:59:59', time())}}').format('D MMMM, YYYY');
                    $('#time_select').html(timeSelect);
                    $('#time_select2').html(timeSelect);
            });
</script>
@stop
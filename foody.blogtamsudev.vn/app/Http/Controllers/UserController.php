<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use Gate;
use Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getIndex()
    {
        $this->authorize('ReadUser');

        return view('childs.user.index');
    }

    public function getUserData()
    {
        $this->authorize('ReadUser');
        $users = User::all();
        return Datatables::of($users)
            ->addColumn('avatar', function ($user) {
                return '<img class="img-circle" alt="User Image" src="' . get_gravatar($user->email) . '"/>';
            })
            ->addColumn('action', function ($user) {
                if ($user->email == auth()->user()->email) {
                    return '';
                }
                $action_btn = '<div class="btn-group">
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            ' . trans('user.action') . ' <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span></button>
                            <ul class="dropdown-menu" role="menu">';
                $action_btn .= '<li><a onclick="editUser(this);" href="#edit-user" data-uid="' . $user->id . '"><i class="fa fa-edit"></i>' . trans('user.edit_user') . '</a></li>';
                if ($user->status == 'Active') {
                    $action_btn .= '<li><a onclick="updateStatusUser(this);" href="#lock-user" data-status="Inactive" data-uid="' . $user->id . '"><i class="fa fa-lock"></i>' . trans('user.lock_user') . '</a></li>';
                } else {
                    $action_btn .= '<li><a onclick="updateStatusUser(this);" href="#unlock-user" data-status="Active" data-uid="' . $user->id . '"><i class="fa fa-unlock"></i>' . trans('user.unlock_user') . '</a></li>';
                }
                $action_btn .= '<li class="divider"></li><li><a onclick="deleteUser(this);" href="#delete-user" data-uid="' . $user->id . '"><i class="fa fa-remove"></i>' . trans('user.delete_user') . '</a></li></ul></div>';
                return $action_btn;
            })
            ->make(true);
    }

    public function getProfile()
    {
        return view('childs.user.profile');
    }

    public function postProfile(Request $request)
    {
        try {
            $status = 'error';
            $msg = trans('user.update_profile_fail');

            $user = auth()->user();
            $user->name = strip_tags($request->get('name'));
            $user->email = strip_tags($request->get('email'));
            if ($request->get('password') !== '') {
                $user->password = bcrypt(strip_tags($request->get('password')));
            }
            $user->description = strip_tags($request->get('description'));
            if ($user->save()) {
                $status = 'success';
                $msg = trans('user.update_profile_success');
            }
            return json_encode(['status' => $status, 'msg' => $msg]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function anyUpdateStatus(Request $request)
    {
        $this->authorize('ActiveUser');

        if (!$request->has('id') || !$request->has('st')) {
            abort(404);
        }

        $editedUser = User::findOrFail($request->get('id'));

        $status = 'error';
        $msg = trans('user.update_status_fail');
        try {
            $editedUser->status = strip_tags($request->get('st'));
            if ($editedUser == 'Normal') {
                $editedUser->user_type = 'Booster';
            }
            if ($editedUser->save()) {
                $status = 'success';
                $msg = trans('user.update_status_success');
            }
            return json_encode(['status' => $status, 'msg' => $msg]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function postDelete(Request $request)
    {
        $this->authorize('DeleteUser');

        if (!$request->has('id')) {
            abort(404);
        }
        $deletedUser = User::findOrFail($request->get('id'));

        $status = 'error';
        $msg = trans('user.delete_fail');
        try {
            if ($deletedUser->delete()) {
                $status = 'success';
                $msg = trans('user.delete_success');
            }
            return json_encode(['status' => $status, 'msg' => $msg]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }

    public function getEdit($user_id)
    {
        $this->authorize('EditUser');

        $user = User::findOrFail($user_id);

        return view('childs.user.edit')->with(['user' => $user]);
    }

    public function postEdit(Request $request)
    {
        $this->authorize('EditUser');

        if (!$request->has('uid') || !$request->has('name') || !$request->has('email')) {
            abort(500);
        }

        $user = User::find($request->get('uid'));

        $status = 'error';
        $msg = trans('user.edit_fail');
        try {
            $user->name = strip_tags($request->get('name'));
            $user->email = strip_tags($request->get('email'));
            $user->user_type = strip_tags($request->get('user_type'));
            $user->description = strip_tags($request->get('description'));
            if ($request->get('password') !== '') {
                $user->password = bcrypt(strip_tags($request->get('password')));
            }
            if ($user->save()) {
                $status = 'success';
                $msg = trans('user.edit_success');
            }
            return json_encode(['status' => $status, 'msg' => $msg]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }

    }

    public function postCreate(Request $request)
    {
        $this->authorize('CreateUser');

        if (!$request->has('name') || !$request->has('email') || !$request->has('password')) {
            abort(500);
        }
        $status = 'error';
        $msg = trans('user.create_fail');
        try {
            $user = new User;
            $user->name = strip_tags($request->get('name'));
            $user->email = strip_tags($request->get('email'));
            $user->user_name = str_slug(strip_tags($request->get('name')));
            $user->user_type = strip_tags($request->get('user_type'));
            $user->description = strip_tags($request->get('description'));
            $user->password = bcrypt(strip_tags($request->get('password')));
            $user->status = 'Active';
            if ($user->save()) {
                $status = 'success';
                $msg = trans('user.create_success');
            }
            return json_encode(['status' => $status, 'msg' => $msg]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }
}

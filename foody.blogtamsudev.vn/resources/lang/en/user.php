<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 2/26/2016
 * Time: 5:47 PM
 */
return [
    'dashboard' => 'Dashboard',
    'list_user' => 'List Users',
    'my_profile' => 'My Profile',
    'about_me' => 'About me',
    'user_name' => 'User Name',
    'name' => 'Full Name',
    'name_placeholder' => 'Enter your full name',
    'email' => 'Email',
    'email_placeholder' => 'Enter your email',
    'description' => 'Description',
    'description_placeholder' => 'Something about you',
    'password' => 'Password',
    'password_placeholder' => 'Only if you want to change',
    'enter_password' => 'Enter your password',
    'user_type' => 'User Type',
    'update_profile_fail' => 'Update Profile Error!',
    'update_profile_success' => 'Update Profile Successful!',
    'user' => 'Users',
    'avatar' => 'Avatar',
    'status' => 'Status',
    'last_login' => 'Last Login',
    'from_ip' => 'From IP',
    'action' => 'Action',
    'edit_user' => 'Edit User',
    'lock_user' => 'Lock User',
    'unlock_user' => 'Unlock User',
    'delete_user' => 'Delete User',
    'not_found' => 'This user not found!',
    'not_permission' => 'You dont have permission!',
    'update_status_fail' => 'Update Status Fail!',
    'update_status_success' => 'Update Status Successful!',
    'delete_success' => 'Delete User Successful!',
    'delete_fail' => 'Delete User Fail!',
    'create_user' => 'Create New User',
    'edit_user' => 'Edit User',
    'change' => 'Change',
    'create_success' => 'Create User Successful!',
    'create_fail' => 'Create User Fail!',
    'edit_success' => 'Edit User Successful!',
    'edit_fail' => 'Edit User Fail!',
];
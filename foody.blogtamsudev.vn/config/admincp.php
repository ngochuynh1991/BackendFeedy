<?php

return [
    'lang_support' => [
        'vi' => 'Tiếng Việt',
        'en' => 'English',
        'la' => 'Laos'
    ],
    'user_type' => [
        'type_1' => 'Admin',
        'type_2' => 'Editor',
        'type_3' => 'Normal',
        'type_4' => 'Guest',
    ]
];